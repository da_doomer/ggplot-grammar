# ggplot-grammar

A grammar for a subset of [ggplot](https://en.wikipedia.org/wiki/Ggplot2). The
grammar is written in the [Extended Backus-Naur
form](https://en.wikipedia.org/wiki/Extended_Backus%E2%80%93Naur_form).

An example parser written on Python with
[Lark](https://github.com/lark-parser/lark) is supplied, with
[plotnine](https://plotnine.readthedocs.io/en/stable/index.html) providing
execution semantics.
