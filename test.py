"""Tests the grammar implementation."""
# Native imports
from pathlib import Path
import tempfile

# External imports
from tqdm import tqdm


programs_path = Path(__file__).parent/"string_examples.txt"
random_program_n = 2


def execute_test(programs: list[str]):
    """Tests that the given programs can be executed."""
    import grammar
    with tempfile.TemporaryDirectory() as temporary_dir:
        for i, program in tqdm(
                tuple(enumerate(programs)),
                desc="Testing execution",
                disable=len(programs) < 2
                ):
            path = Path(temporary_dir)/f"{i}.svg"
            grammar.execute(program, path)
            assert path.exists(), f"{program} cannot be executed!"


def random_program_test(n: int):
    """Tests that random programs can be generated, executed and parsed."""
    import grammar
    for _ in tqdm(range(n), desc="Testing random generation"):
        program = grammar.random_program()
        execute_test([program])


def perturbation_test(n: int):
    """Tests that random programs are perturbed into valid programs."""
    import grammar
    for _ in tqdm(range(n), desc="Testing perturbation"):
        program = grammar.random_program()
        perturbed_program = grammar.perturbed_program(program)
        print(f"Original: {program}\nPerturbed:{perturbed_program}")
        execute_test([perturbed_program])


if __name__ == "__main__":
    print("Testing. This will take a while.")
    with open(programs_path, "rt") as f_p:
        programs = list(map(lambda s: s.strip(), f_p))
    execute_test(programs)
    random_program_test(random_program_n)
    perturbation_test(random_program_n)
    print("All tests completed successfully")
